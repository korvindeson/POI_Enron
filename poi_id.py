#!/usr/bin/python

import sys
import pickle
sys.path.append("../tools/")
from feature_format import featureFormat, targetFeatureSplit
from tester import dump_classifier_and_data
from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVC
from sklearn import grid_search
from sklearn.tree import DecisionTreeClassifier
import numpy as np
### is that backward compatable with v0.17 of sklearn.cross_validation.StratifiedShuffleSplit?
from sklearn.cross_validation import StratifiedShuffleSplit


### Task 1: Select what features you'll use.
### features_list is a list of strings, each of which is a feature name.
### The first feature must be "poi".


### Whole approach to the project would be ease of implementation
### Approach to code should reflect importance and time requirements of the task
### For this project we assume goal of 0.3 for Precision and recall, so we
### can separate large ammount of NOT POIs, so resources can be better allocated
### in the future



# All elements that have to do with pay, and long term profits was selected as
# project backgound report huge profits that POIs had +++ email features in order to create new one for features task
features_list = ['poi','salary','total_payments','bonus','exercised_stock_options','long_term_incentive','shared_receipt_with_poi','from_this_person_to_poi','from_poi_to_this_person']
features_list_finance = ['poi','salary','total_payments','bonus','exercised_stock_options','long_term_incentive','shared_receipt_with_poi']

### Load the dictionary containing the dataset
with open("final_project_dataset.pkl", "rb") as data_file:
    data_dict = pickle.load(data_file)

### Task 2: Remove outliers

emp=[]

ar=[]

i=0

for e in data_dict:
    #print(data_dict[e][0])
    
    ar2=[]
    all_empty=True
    i=0
    skip=True
    for outlier in data_dict[e]:
        #Replace 'NaN' with 0.
        if data_dict[e][outlier] == 'NaN':
            data_dict[e][outlier]=0.

        #count empty and Zero values
        if data_dict[e][outlier] != 0 :
            all_empty=False
        else:
            i+=1
        # if more then 7 features are nothing => remove
        # this improve precision and recall on tester.py by about 0.1 and 0.2
        # i>8 or higher will increase accuracy and decrease precision and recall,
        # so we can manipilate it according to current needs
        if i>7:
            if skip:
                emp.append(e)    
                skip=False
    
    #if no data, mark for delete
    if all_empty:
        emp.append(e)
    

### remove employees with no data and no email to/from POI, since it represents
### no contacts with POI => highly unlikely to be POI
for e in emp:
    data_dict.pop(e,0)


print('Number of people Removed: ',len(emp))
#Delete non human data
data_dict.pop( 'TOTAL', 0 )
data_dict.pop( 'THE TRAVEL AGENCY IN THE PARK', 0 )


        
        
### Store to my_dataset for easy export below.
my_dataset = data_dict


   


### Extract features and labels from dataset for local testing
data = featureFormat(my_dataset, features_list, sort_keys = True)
data_finance =featureFormat(my_dataset, features_list_finance, sort_keys = True)





labels, features = targetFeatureSplit(data)
labelsF, featuresF = targetFeatureSplit(data_finance)


### Task 3: Create new feature(s)


# instead of having from POI or to POI messages. Lets have % of emails that have
# something to do with POI. Otherwise people with large genera number of emails
# might be misrepresented

to_poi=[]
from_poi=[]

for e in data_dict:
    if data_dict[e]['from_messages'] != 0:
        to_poi.append( float(data_dict[e]['from_this_person_to_poi']) / float(data_dict[e]['from_messages']))
    else:
        to_poi.append(0.)
    if data_dict[e]['to_messages'] !=0:
        from_poi.append( float(data_dict[e]['from_poi_to_this_person']) / float(data_dict[e]['to_messages']))
    else:
        to_poi.append(0.)






#replace values in the from_this_person_to_poi and from_poi_to_this_person
for e,t,f in zip(range(0,len(features)-1),to_poi,from_poi):
    features[e][7]=np.array(t)
    features[e][6]=np.array(f)
    



### Task 4: Try a varity of classifiers
### Please name your classifier clf for easy export below.
### Note that if you want to do PCA or other multi-stage operations,
### you'll need to use Pipelines. For more info:
### http://scikit-learn.org/stable/modules/pipeline.html




#dict to store results of classifiers
i={}
        

#initialize classifiers
lr = LogisticRegression()
gnb = GaussianNB()
svc = SVC()


### gridsearch will be used, results of grid search against manual setting will
### be interesting
parameters = {'max_features': ['sqrt', 'log2'],'min_samples_split':[7,8,9,10]}
dctG = grid_search.GridSearchCV(DecisionTreeClassifier(criterion="entropy", random_state=42),parameters)
dct = DecisionTreeClassifier(criterion="entropy", max_features="log2", random_state=42,min_samples_split=10)

### same for Random Forest
parameters = {'n_estimators': [8,9,10]}
rfcG = grid_search.GridSearchCV(RandomForestClassifier(), parameters)
rfc = RandomForestClassifier(max_features='log2',n_estimators=3)





### function to iterate through classifiers with different features
def check_classifiers(features,labels,sss):

    for train_index, test_index in sss:
        
        y_train=[]
        X_train=[]
        y_test=[]
        X_test=[]
        
        for e in train_index:
            y_train.append(labels[e])
            X_train.append(features[e])
        for e in test_index:
            y_test.append(labels[e])
            X_test.append(features[e])
        
        # dimentionality reduction
        #pca = PCA(n_components=2).fit(X_train)
        
        #X_train = pca.transform(X_train)
        #X_test = pca.transform(X_test)
        
        ### loop over classifiers
        for clf, name in [(lr, 'Logistic'),
                          (gnb, 'Naive Bayes'),
                          (svc, 'Support Vector Classification'),
                          (rfcG, 'Random Forest - GridSearch'),
                          (rfc, 'Random Forest'),
                          (dctG, 'Decision Tree - GridSearch'),
                          (dct, 'Decision Tree')]:
            #fit training data
            clf.fit(X_train, y_train)
            
            #predict testing data
            pred=clf.predict(X_test)
           
            #initialize variable
            total_predictions=0
            true_positives=0
            false_positives=0
            false_negatives=0
            true_negatives=0
    
     
            #loop over predictions and labels
            #
            for p,y in zip(pred,y_test):
                if p == 0 and y == 0:
                    true_negatives += 1
                elif p == 0 and y == 1:
                    false_negatives += 1
                elif p == 1 and y == 0:
                    false_positives += 1
                elif p == 1 and y == 1:
                    true_positives += 1
                else:
                    print("O_o") #Just in case
            
                  ### took this from test.py it is just a formulas
            total_predictions = total_predictions + true_negatives + false_negatives + false_positives + true_positives
            accuracy = 1.0*(true_positives + true_negatives)/total_predictions
            if true_positives+false_positives == 0:
                precision = 0
            else:
                precision = 1.0*true_positives/(true_positives+false_positives)
            recall = 1.0*true_positives/(true_positives+false_negatives)
            f1 = 2.0 * true_positives/(2*true_positives + false_positives+false_negatives)
            if (4*precision + recall) == 0:
                f2 = 0
            else:
                f2 = (1+2.0*2.0) * precision*recall/(4*precision + recall)
            
            #store results, average new ones
            if name in i:
                i[name] = { 'accuracy'    : (i[name]['accuracy']+accuracy)/2,
                            'precision'   : (i[name]['precision']+precision)/2,
                            'recall'      : (i[name]['recall']+recall)/2,
                            'f1'          : (i[name]['f1']+f1)/2,
                            'f2'          : (i[name]['f2']+f2)/2               }
            else:
                i[name] = { 'accuracy'    : accuracy,
                            'precision'   : precision,
                            'recall'      : recall,
                            'f1'          : f1,
                            'f2'          : f2       }

    return i









PERF_FORMAT_STRING = "\
\tAccuracy: {:>0.{display_precision}f}\tPrecision: {:>0.{display_precision}f}\t\
Recall: {:>0.{display_precision}f}\tF1: {:>0.{display_precision}f}\tF2: {:>0.{display_precision}f}"

# go over test 1000 times, split test/train 50%/50%, shuffle order of numbers inside
sss = StratifiedShuffleSplit(labels,1000, test_size=0.5, random_state=42)
backup_i= check_classifiers(features,labels,sss)
for c in i:
    print (c, '\n', PERF_FORMAT_STRING.format(i[c]['accuracy'], i[c]['precision'], 
                                              i[c]['recall'], i[c]['f1'], i[c]['f2'], display_precision = 5))

sss = StratifiedShuffleSplit(labelsF,1000, test_size=0.5, random_state=42)
i= check_classifiers(featuresF,labelsF,sss)
for c in i:
    print (c, '\n', PERF_FORMAT_STRING.format(i[c]['accuracy'], i[c]['precision'], 
                                              i[c]['recall'], i[c]['f1'], i[c]['f2'], display_precision = 5))




### Results:
### features with emails
#Logistic 
#        Accuracy: 0.65787       Precision: 0.23744      Recall: 0.25855 F1: 0.24581     F2: 0.25305
#Naive Bayes 
#        Accuracy: 0.68241       Precision: 0.22722      Recall: 0.28843 F1: 0.24936     F2: 0.27029
#Support Vector Classification 
#        Accuracy: 0.78125       Precision: 0.00000      Recall: 0.00000 F1: 0.00000     F2: 0.00000
#Random Forest - GridSearch 
#        Accuracy: 0.78037       Precision: 0.65631      Recall: 0.12795 F1: 0.20428     F2: 0.14967
#Random Forest 
#        Accuracy: 0.71916       Precision: 0.24332      Recall: 0.21659 F1: 0.22170     F2: 0.21747
#Decision Tree - GridSearch 
#        Accuracy: 0.64111       Precision: 0.29021      Recall: 0.46548 F1: 0.35443     F2: 0.41245
#Decision Tree 
#        Accuracy: 0.68209       Precision: 0.28527      Recall: 0.34299 F1: 0.30939     F2: 0.32822

### features with only finance
#Logistic 
#        Accuracy: 0.65787       Precision: 0.23744      Recall: 0.25855 F1: 0.24581     F2: 0.25305
#Naive Bayes 
#        Accuracy: 0.68241       Precision: 0.22722      Recall: 0.28843 F1: 0.24936     F2: 0.27029
#Support Vector Classification 
#        Accuracy: 0.78125       Precision: 0.00000      Recall: 0.00000 F1: 0.00000     F2: 0.00000
#Random Forest - GridSearch 
#        Accuracy: 0.73512       Precision: 0.23461      Recall: 0.17615 F1: 0.19678     F2: 0.18350
#Random Forest 
#        Accuracy: 0.72568       Precision: 0.16662      Recall: 0.10144 F1: 0.12575     F2: 0.10989
#Decision Tree - GridSearch 
#        Accuracy: 0.70985       Precision: 0.37348      Recall: 0.45398 F1: 0.40080     F2: 0.42894
#Decision Tree 
#        Accuracy: 0.71331       Precision: 0.36357      Recall: 0.38242 F1: 0.36657     F2: 0.37445


### Conclusions:
    ### Logistic - highest precision, if person is identified as POI,
    ###            there is a highest chance that he is a POI
    ### Naive Bayes - surprizingly good considering default settings. If goal
    ###    of the project would be highest accuracy would be a good candidate.
    ###    However, iterations with averaging Recalls in for loop might be the
    ###    reason for high recall. But percicion should work.
    ###
    ### Support Vector Classification - breaks on default parameters
    ###
    ### Random Forest & GridSearch - bit human settings, winner for both 
    ###                             precision and recall
    ### Decision Tree & GridSearch - gridsearch did not bring much with current
    ###            and more extensive parameters. Since it is also the fastest
    ###            and bedt performing for test.py it will be passed in the end
    ###
    ### Choice --> Decision Tree with GridSearch to tune parameters is clear
    ###            winner on all ratios
############################################################################
    ### Feature choice - features without email information have the best ratios




parameters = {'max_features': ['sqrt', 'log2'],'min_samples_split':[7,8,9,10]}
clf = grid_search.GridSearchCV(DecisionTreeClassifier(criterion="entropy", random_state=42),parameters)

features_list=features_list_finance


#I fit all features, right?
clf.fit(featuresF, labelsF)

# Example starting point. Try investigating other evaluation techniques!
#from sklearn.cross_validation import train_test_split
#features_train, features_test, labels_train, labels_test = \
#    train_test_split(features, labels, test_size=0.3, random_state=42)


####### on top is the example of function, that is same as SSS, that I used
####### but with less options, so I ignored


### Task 6: Dump your classifier, dataset, and features_list so anyone can
### check your results. You do not need to change anything below, but make sure
### that the version of poi_id.py that you submit can be run on its own and
### generates the necessary .pkl files for validating your results.

dump_classifier_and_data(clf, my_dataset, features_list)