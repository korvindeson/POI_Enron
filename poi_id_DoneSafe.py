#!/usr/bin/python

import sys
import pickle
sys.path.append("../tools/")
from feature_format import featureFormat, targetFeatureSplit
from tester import dump_classifier_and_data

### Task 1: Select what features you'll use.
### features_list is a list of strings, each of which is a feature name.
### The first feature must be "poi".
#features_list = ['poi','salary','deferral_payments','total_payments','bonus','total_stock_value','from_poi_to_this_person','exercised_stock_options','from_this_person_to_poi','long_term_incentive','shared_receipt_with_poi'] # You will need to use more features
features_list = ['poi','salary','total_payments','bonus','exercised_stock_options','long_term_incentive']

### Load the dictionary containing the dataset
with open("final_project_dataset.pkl", "rb") as data_file:
    data_dict = pickle.load(data_file)

### Task 2: Remove outliers
for e in data_dict:
    for outlier in data_dict[e]:
        if data_dict[e][outlier] == 'NaN':
            data_dict[e][outlier]=0.


data_dict.pop( 'TOTAL', 0 )
data_dict.pop( 'THE TRAVEL AGENCY IN THE PARK', 0 )

        
        
        
### Task 3: Create new feature(s)
### Store to my_dataset for easy export below.
my_dataset = data_dict



### Extract features and labels from dataset for local testing
data = featureFormat(my_dataset, features_list, sort_keys = True)
labels, features = targetFeatureSplit(data)



### Task 4: Try a varity of classifiers
### Please name your classifier clf for easy export below.
### Note that if you want to do PCA or other multi-stage operations,
### you'll need to use Pipelines. For more info:
### http://scikit-learn.org/stable/modules/pipeline.html

# Provided to give you a starting point. Try a variety of classifiers.
from sklearn.naive_bayes import GaussianNB
clf = GaussianNB()

from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVC
from sklearn import grid_search
from sklearn.metrics import accuracy_score

from sklearn.model_selection import train_test_split

lr = LogisticRegression()
gnb = GaussianNB()
svc = SVC()                                                  #SVC(C=1.0)
rfc = RandomForestClassifier ()                              #(n_estimators=100)


from sklearn.decomposition import PCA
from sklearn.preprocessing import MinMaxScaler

#features = MinMaxScaler().fit_transform(features)

X_train, X_test, y_train, y_test = train_test_split(features, labels, test_size=0.25, random_state=42)


pca = PCA(n_components=2).fit(X_train)

X_train = pca.transform(X_train)
X_test = pca.transform(X_test)

X_train=MinMaxScaler().fit_transform(X_train)
X_test=MinMaxScaler().fit_transform(X_test)

acc_winner=''
i=0


for clf, name in [(lr, 'Logistic'),
                  (gnb, 'Naive Bayes'),
                  (svc, 'Support Vector Classification'),
                  (rfc, 'Random Forest')]:
    
    clf.fit(X_train, y_train)
    pred=clf.predict(X_test)
    
    
    if i < accuracy_score(pred,y_test):
        acc_winner=name
        clfB=clf.fit(X_train, y_train)
        i=accuracy_score(pred,y_test)
        print (name, " ", i)

print ('\nBest in prediction with default settings: ',acc_winner)

#did not tune this part, since it is already established, that
#way to go is "Support Vector Classification"

#parameters={'kernel':( 'linear', 'poly', 'rbf', 'sigmoid', 'precomputed'), 'C':[1, 10], 'class_weight':['', 'balanced'] }
#parameters={'kernel':( 'linear', 'rbf'), 'C':[1, 10], 'class_weight':['', 'balanced']}
#param_grid = {'C': [1, 10], 'gamma': [0.0001, 0.0005, 0.001, 0.005, 0.01, 0.1] }
#clf = grid_search.GridSearchCV(SVC(kernel='rbf', class_weight=''), parameters)
#clf = SVC(kernel='rbf', C=1,gamma=0.001)

#parameters = {'n_estimators': [4,5,8]}
#clf = grid_search.GridSearchCV(RandomForestClassifier(), parameters)
#clf = RandomForestClassifier(bootstrap=True, class_weight='balanced', criterion='gini',
#            max_depth=None, max_features='auto', max_leaf_nodes=None,
#            min_impurity_split=1e-07, min_samples_leaf=1,
#            min_samples_split=2, min_weight_fraction_leaf=0.0,
#            n_estimators=3, n_jobs=1, oob_score=False, random_state=None,
#            verbose=0, warm_start=False)
#clf=grid_search.GridSearchCV(clfB, parameters)
#print(clf)



from sklearn.tree import DecisionTreeClassifier

#parameters = {'max_features': ['sqrt', 'log2'],'min_samples_split':[3,4]}
#clf = grid_search.GridSearchCV(DecisionTreeClassifier(criterion="entropy", max_features="auto", random_state=42),parameters)
clf = DecisionTreeClassifier(criterion="entropy", max_features="auto", random_state=42,min_samples_split=8)

#clf = GaussianNB()


#param_grid = {'C': [1,2,5,8,100, 10], 'class_weight':['', 'balanced'],'intercept_scaling':[0.5,1,1.5,5,8] }
#clf=grid_search.GridSearchCV(LogisticRegression(),param_grid)








clf.fit(X_train, y_train)
pred=clf.predict(X_test)



for p,t in zip(pred,y_test):
    print(p,t)
print(accuracy_score(pred,y_test))

### Task 5: Tune your classifier to achieve better than .3 precision and recall 
### using our testing script. Check the tester.py script in the final project
### folder for details on the evaluation method, especially the test_classifier
### function. Because of the small size of the dataset, the script uses
### stratified shuffle split cross validation. For more info: 
### http://scikit-learn.org/stable/modules/generated/sklearn.cross_validation.StratifiedShuffleSplit.html

# Example starting point. Try investigating other evaluation techniques!
from sklearn.cross_validation import train_test_split
features_train, features_test, labels_train, labels_test = \
    train_test_split(features, labels, test_size=0.3, random_state=42)

### Task 6: Dump your classifier, dataset, and features_list so anyone can
### check your results. You do not need to change anything below, but make sure
### that the version of poi_id.py that you submit can be run on its own and
### generates the necessary .pkl files for validating your results.

dump_classifier_and_data(clf, my_dataset, features_list)